import * as React from 'react';
import { View, Text, SafeAreaView, ScrollView } from 'react-native';
import { CustomHeader } from '../../Components/Header'
import { CustomTabBar } from '../../Components/CutomTabBar'
import { DetailsScreen } from '../../Components/Detailscreen'
import { Colors, Helpers, Fonts } from '../../Theme'
import styles from './styles'
const tabBarOption = [
  { id: 1, name: 'Details' },
  { id: 2, name: 'Tranport' },
  { id: 3, name: 'Hotel' },
  { id: 4, name: 'Advance' },
  { id: 5, name: 'History' }
]
export class TripDetailContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = { currentTab: { id: 1, name: 'Details' } }
  }

  handleTabChange = (newTabIndex) => {
    this.setState({ currentTab: newTabIndex });
  }
  render() {
    return (
      <SafeAreaView style={Helpers.fill}>
        <CustomHeader
          title='TRIP DETAILS'
          goBack={this.props.navigation.goBack}
        />
        <CustomTabBar
          tabOptions={tabBarOption}
          currentTab={this.state.currentTab.id}
          handleTabChange={this.handleTabChange}
        />
        {this.state.currentTab.id == 1 && <DetailsScreen />}
        {this.state.currentTab.id !== 1 && <View style={Helpers.fillCenter}>
          <Text>{this.state.currentTab.name}</Text>
        </View>}
      </SafeAreaView>
    );
  }
}

export default TripDetailContainer;

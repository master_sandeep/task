import * as React from 'react';
import { View, Keyboard, Text, Button, TouchableOpacity, StatusBar, Image, TextInput, SafeAreaView } from 'react-native';

import NavigationService from 'App/Services/NavigationService'
import {  Helpers, Images, Colors } from '../../Theme'
export class HomeContainer extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView>
        <View style={Helpers.center}>
          <TouchableOpacity style={{backgroundColor: Colors.primary, width: 100, height: 50, alignItems:'center',  justifyContent:'center', marginTop: 300 }} onPress={()=>{NavigationService.navigate('TripDetailsScreen')}} >
            <Text>Trip Details</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

export default HomeContainer;

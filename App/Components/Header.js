import React from 'react';
import { View, Text, TouchableOpacity, Dimensions, StatusBar } from 'react-native';
import { Colors, Helpers } from '../Theme'
import { Icon } from 'react-native-elements'
import styles from './styles'

export const CustomHeader = (props) => {
    return (
        <View style={styles.customHeaderConatiner}>
            <StatusBar backgroundColor={Colors.statusBar} />
            <View style={{ width: 60 }}>
                {props.goBack &&
                    <TouchableOpacity style={styles.customHeaderSideConatiner} onPress={() => { props.goBack(null) }}>
                        <Icon name='arrowleft' type='antdesign' size={30} color={Colors.white} />
                    </TouchableOpacity>}
            </View>
            <View style={[Helpers.center, { width: Dimensions.get('window').width - 120 }]}>
                <Text style={styles.customHeaderTitle}>{props.title}</Text>
            </View>
            <View style={{ width: 60 }}>
                <TouchableOpacity style={styles.customHeaderSideConatiner} onPress={() => { alert('more options') }}>
                    <Icon name='dots-vertical' type='material-community' size={30} color={Colors.white} />
                </TouchableOpacity>
            </View>
        </View>
    );
}

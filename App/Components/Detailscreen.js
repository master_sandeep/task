import * as React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Colors, Helpers, Fonts } from '../Theme'
import styles from './styles'

export const DetailsScreen = () => {
    return (
        <>
            <ScrollView style={[styles.scrollView]}>
                <View style={styles.userDeatilConatiner}>
                    <View style={[{ flex: .20 }, Helpers.center]}>
                        <View style={styles.userImage} />
                    </View>
                    <View style={[{ flex: .40, justifyContent: 'center' }]}>
                        <Text style={styles.employeeDetailHeading}>Employee</Text>
                        <Text style={styles.employeeDetailContent}>Bhawin Khabra</Text>
                    </View>
                    <View style={[{ flex: .40, justifyContent: 'center' }]}>
                        <Text style={styles.employeeDetailHeading}>Reporting Manager</Text>
                        <Text style={styles.employeeDetailContent}>Unaasign</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.deatilRowConatiner}>
                        <View style={{ flex: .60 }}>
                            <Text style={styles.detailHeading}>PROJECT NAME</Text>
                            <Text style={styles.detailContent}>Marketing</Text>
                        </View>
                        <View style={{ flex: .40 }}>
                            <Text style={styles.detailHeading}>Status</Text>
                            <Text style={styles.detailContent}>Submitted</Text>
                        </View>
                    </View>
                    <View style={styles.deatilRowConatiner}>
                        <View style={{ flex: .60 }}>
                            <Text style={styles.detailHeading}>SUBMITTED ON</Text>
                            <Text style={styles.detailContent}>12 Jan 2020</Text>
                        </View>
                        <View style={{ flex: .40 }}>
                            <Text style={styles.detailHeading}>TIME</Text>
                            <Text style={styles.detailContent}>3:12 PM</Text>
                        </View>
                    </View>
                    <View style={styles.deatilRowConatiner}>
                        <View style={{ flex: .60 }}>
                            <Text style={styles.detailHeading}>START DATE</Text>
                            <Text style={styles.detailContent}>04 Jan 2020</Text>
                        </View>
                        <View style={{ flex: .40 }}>
                            <Text style={styles.detailHeading}>END DATE</Text>
                            <Text style={styles.detailContent}>11 Jan 2020</Text>
                        </View>
                    </View>
                    <View style={styles.deatilRowConatiner}>
                        <View style={{ flex: .60 }}>
                            <Text style={styles.detailHeading}>TRIP ID</Text>
                            <Text style={styles.detailContent}>T-02-20-0002</Text>
                        </View>
                        <View style={{ flex: .40 }}>
                            <Text style={styles.detailHeading}>SELECTED TRAVELERS</Text>
                            <Text style={styles.detailContent}>Amit Shah</Text>
                        </View>
                    </View>
                    <View style={[styles.deatilRowConatiner, { marginBottom: 120 }]}>
                        <View style={{ flex: .60 }}>
                            <Text style={styles.detailHeading}>DESTINATION</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={styles.firstCity}><Text style={styles.cityText}>Delhi</Text></View>
                                <View style={styles.secondCity}><Text style={styles.cityText}>Agra</Text></View>
                            </View>
                            <View style={styles.thirdCity}><Text style={styles.cityText}>Lucknow</Text></View>
                        </View>
                        <View style={{ flex: .40 }}>
                            <Text style={styles.detailHeading}>PURPOSE</Text>
                            <Text style={styles.detailContent}>Sale</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <View style={Helpers.fillHalf}>
                    <Text style={Fonts.normal}>Return</Text>
                </View>
                <View style={Helpers.fillHalf}>
                    <Text style={[Fonts.normal, { color: Colors.primary }]}>Approve</Text>
                </View>
            </View>
        </>
    );
}
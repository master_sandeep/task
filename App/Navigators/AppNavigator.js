import React from 'react'
import { createAppContainer, createStackNavigator } from 'react-navigation'
import SplashScreen from 'App/Containers/SplashScreen/SplashScreen'
import HomeScreen from '../Containers/HomeScreen'
import TripDetailsScreen from '../Containers/TripDetailsScreen'

/**
* Home Screen Bottom TabBar navigation
*/
const MainStackNavigator = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
    },
    TripDetailsScreen: {
      screen: TripDetailsScreen,
     
    },
  },
  {
    initialRouteName: 'HomeScreen',
    headerMode: 'none',
  }
);

const StackNavigator = createStackNavigator(
  {
    SplashScreen: SplashScreen,
    MainScreen: MainStackNavigator
  },
  {
    // By default the application will show the splash screen
    initialRouteName: 'SplashScreen',
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: 'none',
  }
)

export default createAppContainer(StackNavigator)

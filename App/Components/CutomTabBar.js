import React from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions, StatusBar, FlatList } from 'react-native';
import { Images, FontName, Colors } from '../Theme'
import { Icon } from 'react-native-elements'
import styles from './styles';

export const CustomTabBar = (props) => {
    return (
        <FlatList
            horizontal
            contentContainerStyle={styles.customTabConatiner}
            keyExtractor={item => 'GenresHome' + item.id.toString()}
            data={props.tabOptions}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) =>
                <View style={{ marginHorizontal: 0, padding: 20, borderBottomColor: Colors.primary, borderBottomWidth: item.id == props.currentTab ? 2 : 0 }}>
                    <TouchableOpacity onPress={() => { props.handleTabChange(item) }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: item.id == props.currentTab ? Colors.primary : Colors.grey }}>{item.name}</Text>
                    </TouchableOpacity>
                </View>
            }
        />
    );
}

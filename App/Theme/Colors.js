/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  transparent: 'rgba(0,0,0,0)',
  white: '#ffffff',
  text: '#212529',
  primary: '#1c30e8',
  grey: '#b1b5b5',
  success: '#28a745',
  error: '#dc3545',
  statusBar:"#1b2ab3"
}

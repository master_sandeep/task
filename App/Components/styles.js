import { StyleSheet } from 'react-native';
import { Colors, FontName } from '../Theme'

export default styles = StyleSheet.create({
    customTabConatiner: {
        marginLeft: 10,
        height: 50,
        marginBottom: 20
    },
    customTabItemConatiner: {
        marginHorizontal: 0,
        padding: 20,
        borderBottomColor: Colors.primary
    },
    customTabTitle: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    customHeaderConatiner: {
        flexDirection: 'row',
        height: 60,
        backgroundColor: Colors.primary
    },
    customHeaderSideConatiner: {
        alignContent: 'center',
        alignItems: 'center',
        height: 60,
        width: 60,
        justifyContent: 'center'
    },
    customHeaderTitle: {
        fontSize: 20,
        fontFamily: FontName.Montserrat_Medium,
        textAlign: 'center',
        color: Colors.white
    },
    scrollView: {
        backgroundColor: Colors.white,
    },
    userDeatilConatiner: {
        flexDirection: 'row',
        backgroundColor: '#f0f2f2',
        borderRadius: 10,
        margin: 20,
        borderWidth: 1,
        borderColor: Colors.grey,
        height: 70
    },
    deatilRowConatiner: {
        flexDirection: 'row',
        margin: 20
    },
    detailHeading: {
        fontSize: 14,
        opacity: .50
    },
    detailContent: {
        fontSize: 18,
        marginTop: 10
    },
    firstCity: {
        backgroundColor: '#78d69e',
        paddingVertical: 3,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    secondCity: {
        backgroundColor: '#db5a7a',
        marginLeft: 10,
        paddingVertical: 3,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    thirdCity: {
        backgroundColor: '#7ebecf',
        paddingVertical: 3,
        paddingHorizontal: 10,
        marginTop: 10,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    cityText: {
        fontSize: 16,
        color: Colors.white
    },
    footer: {
        flexDirection: 'row',
        height: 70,
        borderColor: Colors.grey,
        borderWidth: 1
    },
    userImage: {
        backgroundColor: 'red',
        borderRadius: 25,
        width: 50,
        height: 50
    },
    employeeDetailHeading: {
        fontSize: 16,
        fontWeight: '900'
    },
    employeeDetailContent: {
        fontSize: 16,
        fontWeight: 'bold'
    }
})
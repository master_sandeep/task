import { put, delay } from 'redux-saga/effects'
import NavigationService from 'App/Services/NavigationService'
import SplashScreen from 'react-native-splash-screen'
/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */
export function* startup() {
  NavigationService.navigateAndReset('MainScreen')
  yield delay(2000)
  SplashScreen.hide()
}
